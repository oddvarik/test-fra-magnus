package Arv;

public class Animal {
    protected String name;
    protected int noOfLegs;
    protected boolean vegetarian;

    public Animal(String name, int noOfLegs, boolean vegetarian){
        this.name = name;
        this.noOfLegs = noOfLegs;
        this.vegetarian = vegetarian;
    }

    public String toString(){
        return "Name: " + name + " Number of legs: " + noOfLegs + " Is vegetarian? " + vegetarian;
    }

    public String getName() {
        return name;
    }

    public int getNoOfLegs() {
        return noOfLegs;
    }

    public boolean isVegetarian() {
        return vegetarian;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNoOfLegs(int noOfLegs) {
        this.noOfLegs = noOfLegs;
    }

    public void setVegetarian(boolean vegetarian) {
        this.vegetarian = vegetarian;
    }
}
