package Arv;

public class Dog extends Animal{

    private String color;

    public Dog(String name, int noOfLegs, boolean vegetarian, String color){
        super(name, noOfLegs, vegetarian);
        this.color = color;
    }

    public String toString(){
        return super.toString() + " Color: " + color;
    }
}
