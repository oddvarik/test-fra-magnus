package Arv;

public class Cat extends Animal{

    private String farge;

    public Cat(String name, int noOfLegs, boolean vegetarian, String farge){
        super(name, noOfLegs, vegetarian);
        this.farge = farge;
    }

    public String toString(){
        return super.toString() + " Farge: " + farge;
    }
}
