import Object.*;
import Arv.*;

public class main {
    public static void main(String[] args) {
        /*Car car1 = new Car("Red", "Volvo");*/
        /*Carr car2 = new Car("Blue", "Opel");*/
        /*System.out.println(car1.toString());*/
        /*System.out.println();*/
        /*System.out.println(car2.toString());*/

        Cat cat = new Cat("Ola", 4, false, "Rosa");
        Dog dog = new Dog("Stella", 4, false, "Svart");
        Dog hund = new Dog("Kalle", 5, true, "Gold");
        var katt = new Cat("Blinks", 3, true, "Svart"); /*lovlig*/

        System.out.println(cat.toString());
        System.out.println(dog.toString());
        System.out.println(hund.toString());
        System.out.println(katt.toString());

        dog.setName("Allets");
        hund.setVegetarian(false);

        System.out.println("\n Etter endring \n");
        System.out.println(dog.toString());
        System.out.println(hund.toString());
    }
}

