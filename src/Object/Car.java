package Object;

public class Car {
    private String Color;
    private String Brand;

    public Car(String Color, String Brand){
        this.Color = Color;
        this.Brand = Brand;
    }

    public String toString(){
        return "Color: " + Color + " Brand: " + Brand;
    }
}
